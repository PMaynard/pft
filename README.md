# Pete's Fancy Typer (pft)

A simple text editor using [Qt](https://www.qt.io/) and trying to follow some of the principles from [Better Web Type](https://betterwebtype.com/rhythm-in-web-typography).

![wip screenshot](screeny.png)

# Action plan

- [ ] Basic Text editor features
- [ ] Prettifying
- [ ] Extra nice things
	- [ ] Synatx highlighting
	- [ ] Version control
	- [ ] Export options
		- [ ] PDF
		- [ ] HTML
	- [ ] Cross Platform

# Development

QT Designer is used to create develop the Qt Quck QML. This can be installed:

	apt-get install qtcreator 

Configure a virtual environment

    python3 -m venv venv
    . venv/bin/activate

Install dependencies

    pip install pyside2