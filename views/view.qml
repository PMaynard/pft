import QtQuick 2.9

GridView {
    id: gridView
    anchors.fill: parent
    flickDeceleration: 1501
    maximumFlickVelocity: 2501
    layoutDirection: Qt.LeftToRight
    delegate: Item {
        x: 5
        height: 50
        Column {
            spacing: 5
            Rectangle {
                width: 40
                height: 40
                color: colorCode
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Text {
                x: 5
                text: name
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold: true
            }
        }
    }
    cellWidth: 70
    model: ListModel {
        ListElement {
            name: "Grey"
            colorCode: "grey"
        }

        ListElement {
            name: "Red"
            colorCode: "red"
        }

        ListElement {
            name: "Blue"
            colorCode: "blue"
        }

        ListElement {
            name: "Green"
            colorCode: "green"
        }
        ListElement {
            name: "Orange"
            colorCode: "orange"
        }
    }
    cellHeight: 70
}